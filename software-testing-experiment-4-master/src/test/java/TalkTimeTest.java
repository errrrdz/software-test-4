import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TalkTimeTest {
    @DisplayName(value = "决策表测试用例")
    @ParameterizedTest
    @CsvFileSource( resources = "/决策表测试用例.csv",numLinesToSkip = 1,encoding = "UTF-8")
    void decisionTableTesting(String StartTime,String EndTime,String Expected)throws ParseException{
        String dateCalculation = TalkTime.dateCalculation(StartTime, EndTime);
        assertEquals(Expected,dateCalculation);
    }

}
